package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import no.uib.ii.inf112.pond.Pond;
import no.uib.ii.inf112.pond.Position;
import no.uib.ii.inf112.pond.impl.Duck;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PondTest {

    @Test
    void testPondNotNull(){
        Pond pond = new Pond(1280, 720);
        assertNotNull(pond);
    }

    @Test
    void testDuckNotNull(){
        Duck duck = new Duck(Position.create(0, 0),25);
        assertNotNull(duck);
    }
    @Test
    void testMovingDuck() {
        Pond pond = new Pond(1280, 720);
        Duck duck = (Duck) pond.objs.get(0);

        for (int i = 0; i < 69; i++){
            double startX = duck.getX();
            pond.step();
            double endX = duck.getX();
            assertEquals(startX, endX + 1);

        }
    }
}
